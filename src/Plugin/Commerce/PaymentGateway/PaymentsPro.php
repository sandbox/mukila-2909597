<?php

namespace Drupal\commerce_paypal_wpp\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * Provides the Paypal PaymentsPro payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paypal_payments_pro",
 *   label = "PayPal (PaymentsPro)",
 *   display_label = "PayPal Payments Pro",
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "discover", "mastercard", "visa",
 *   },
 * )
 */
class PaymentsPro extends OnsitePaymentGatewayBase implements PaymentsProInterface {

  /**
   * Paypal test API URL.
   */
  const PAYPAL_API_TEST_URL = 'https://api-3t.sandbox.paypal.com/nvp';

  /**
   * Paypal production API URL.
   */
  const PAYPAL_API_URL = 'https://api-3t.paypal.com/nvp';

  /**
   * Paypal production API URL.
   */
  const PAYPAL_API_VERSION = '76.0';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientInterface $client, RounderInterface $rounder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->httpClient = $client;
    $this->rounder = $rounder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('http_client'),
      $container->get('commerce_price.rounder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_username' => '',
      'api_password' => '',
      'signature' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Username'),
      '#default_value' => $this->configuration['api_username'],
      '#required' => TRUE,
    ];
    $form['api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('API Password'),
      '#default_value' => $this->configuration['api_password'],
      '#required' => empty($this->configuration['api_password']),
    ];
    $form['signature'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signature'),
      '#default_value' => $this->configuration['signature'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_username'] = $values['api_username'];
      $this->configuration['api_password'] = $values['api_password'];
      $this->configuration['signature'] = $values['signature'];
      if (!empty($values['api_password'])) {
        $this->configuration['api_password'] = $values['api_password'];
      }
    }
  }

  /**
   * Returns the Api URL.
   */
  protected function getApiUrl() {
    return $this->getMode() == 'test' ? self::PAYPAL_API_TEST_URL : self::PAYPAL_API_URL;
  }

  /**
   * Returns the vendor.
   */
  protected function getSignature() {
    return $this->configuration['signature'] ?: '';
  }

  /**
   * Returns the user.
   */
  protected function getUsername() {
    return $this->configuration['api_username'] ?: '';
  }

  /**
   * Returns the password.
   */
  protected function getPassword() {
    return $this->configuration['api_password'] ?: '';
  }

  /**
   * Format the expiration date for Payflow from the provided payment details.
   *
   * @param array $payment_details
   *   The payment details array.
   *
   * @return string
   *   The expiration date string.
   */
  protected function getExpirationDate(array $payment_details) {
    return $payment_details['expiration']['month'] . $payment_details['expiration']['year'];
  }

  /**
   * Merge default Payflow parameters in with the provided ones.
   *
   * @param array $parameters
   *   The parameters for the transaction.
   *
   * @return array
   *   The new parameters.
   */
  protected function getParameters(array $parameters = []) {
    $defaultParameters = [
      'user' => $this->getUsername(),
      'pwd' => $this->getPassword(),
      'signature' => $this->getSignature(),
      'version' => self::PAYPAL_API_VERSION, // @TODO: Make sure it's is static value.
    ];

    return $parameters + $defaultParameters;
  }
  /**
   * Post a transaction to the Payflow server and return the response.
   *
   * @param array $parameters
   *   The parameters to send (will have base parameters added).
   *
   * @return array
   *   The response body data in array format.
   */
  protected function executeTransaction(array $parameters) {
    $body = $this->prepareBody($parameters);
    $response = $this->httpClient->post($this->getApiUrl(), [
      'headers' => [
        'Content-Type' => 'text/namevalue',
        'Content-Length' => strlen($body),
      ],
      'body' => $body,
      'timeout' => 30,
    ]);
    return $this->prepareResult($response->getBody()->getContents());
  }

  /**
   * Prepares the result of a request.
   *
   * @param string $body
   *   The result.
   *
   * @return array
   *   An array of the result values.
   */
  protected function prepareResult($body) {
    $responseParts = explode('&', $body);

    $result = [];
    foreach ($responseParts as $bodyPart) {
      list($key, $value) = explode('=', $bodyPart, 2);
      $result[strtolower($key)] = urldecode($value);
    }
    return $result;
  }

  /**
   * Prepares the request body to name/value pairs.
   *
   * @param array $parameters
   *   The request parameters.
   *
   * @return string
   *   The request body.
   */
  protected function prepareBody(array $parameters = []) {
    $parameters = $this->getParameters($parameters);

    $values = [];
    foreach ($parameters as $key => $value) {
      $values[] = strtoupper($key) . '=' . $value;
    }

    return implode('&', $values);
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {

  }

  /**
   * {@inheritdoc}
   *
   * TODO: Find a way to store the capture ID.
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    if ($payment->getCompletedTime() < strtotime('-180 days')) {
      throw new InvalidRequestException("Unable to refund a payment captured more than 180 days ago.");
    }
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $amount = $this->rounder->round($amount);
    $this->assertRefundAmount($payment, $amount);
    if (empty($payment->getRemoteId())) {
      throw new InvalidRequestException('Could not determine the remote payment details.');
    }
    
    try {
      $new_refunded_amount = $payment->getRefundedAmount()->add($amount);
      $data = $this->executeTransaction([
        'method' => 'RefundTransaction',
        'transactionid' => $payment->getRemoteId(),
        'amt' => $new_refunded_amount,
        'refundtype' => 'Partial',
        'currencycode' => $payment->getAmount()->getCurrencyCode(),
      ]);

      if (in_array($data['ack'], ['Failure', 'FailureWithWarning'])) {
        throw new PaymentGatewayException('Credit could not be completed. Message: ' . $data['l_longmessage0'], $data['l_errorcode0']);
      }

      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      }
      else {
        $payment->setState('refunded');
      }

      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();
    }
    catch (RequestException $e) {
      throw new InvalidRequestException("Could not refund the payment.", $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // Ensure we can determine a valid IPv4 IP address as required by PayPal WPP.
    $ip_address = $_SERVER['REMOTE_ADDR'];
    // Go ahead and convert localhost from IPv6 to IPv4.
    $ip_address = ($ip_address == '::1') ? '127.0.0.1' : $ip_address;
    try {
      /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
      $address = $payment_method->getBillingProfile()->get('address')->first();
      $data = $this->executeTransaction([
        'paymentaction' => 'Authorization',
        'method' => 'DoDirectPayment',
        'creditcardtype' => $payment_details['type'],
        'acct' => $payment_details['number'],
        'expdate' => $this->getExpirationDate($payment_details),
        'cvv2' => $payment_details['security_code'],
        'amt' => 20,
        'ipaddress' => $ip_address,
        'currenycode' => 'USD',
        'email' => $payment_method->getOwner()->getEmail(),
        'firstname' => $address->getGivenName(),
        'lastname' => $address->getFamilyName(),
        'street' => $address->getAddressLine1(),
        'street2' => $address->getAddressLine2(),
        'city' => $address->getLocality(),
        'state' => $address->getAdministrativeArea(),
        'zip' => $address->getPostalCode(),
        'countrycode' => $address->getCountryCode(),
      ]);

      if (in_array($data['ack'], ['Failure', 'FailureWithWarning'])) {
        throw new HardDeclineException("Unable to verify the credit card: " . $data['l_longmessage0'], $data['l_errorcode0']);
      }

      $payment_method->card_type = $payment_details['type'];
      // Only the last 4 numbers are safe to store.
      $payment_method->card_number = substr($payment_details['number'], -4);
      $payment_method->card_exp_month = $payment_details['expiration']['month'];
      $payment_method->card_exp_year = $payment_details['expiration']['year'];
      $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
      // Store the remote ID returned by the request.
      $payment_method
        ->setRemoteId($data['transactionid'])
        ->setExpiresTime($expires)
        ->save();
    }
    catch (RequestException $e) {
      throw new HardDeclineException("Unable to store the credit card");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->validatePayment($payment, 'new');
    // Ensure we can determine a valid IPv4 IP address as required by PayPal WPP.
    $ip_address = $_SERVER['REMOTE_ADDR'];
    // Go ahead and convert localhost from IPv6 to IPv4.
    $ip_address = ($ip_address == '::1') ? '127.0.0.1' : $ip_address;
    try {
      $data = $this->executeTransaction([
        'paymentaction' => 'Sale',
        'method' => 'DoReferenceTransaction',
        'referenceid' => $payment->getPaymentMethod()->getRemoteId(),
        'ipaddress' => $ip_address,
        'amt' => $this->rounder->round($payment->getAmount())->getNumber(),
        'currencycode' => $payment->getAmount()->getCurrencyCode(),
        'invnum' => substr($payment->getOrderId(), 0, 127) . '-' . REQUEST_TIME,
        'custom' => substr($this->t('Order @number', array('@number' => $payment->getOrderId())), 0, 256),
        'completetype' => 'Complete',
      ]);
       if (in_array($data['ack'], ['Failure', 'FailureWithWarning'])) {
        throw new HardDeclineException('Could not charge the payment method. Response: ' . $data['l_longmessage0'], $data['l_errorcode0']);
      }

      $next_state = $capture ? 'completed' : 'authorization';
      $payment->setState($next_state);
      if (!$capture) {
        $payment->setExpiresTime($this->time->getRequestTime() + (86400 * 29));
      }
      $payment
        ->setRemoteId($data['transactionid'])
        ->setRemoteState('3')
        ->save();
    }
    catch (RequestException $e) {
      throw new HardDeclineException('Could not charge the payment method.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * Attempt to validate payment information according to a payment state.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to validate.
   * @param string|null $payment_state
   *   The payment state to validate the payment for.
   */
  protected function validatePayment(PaymentInterface $payment, $payment_state = 'new') {
    $this->assertPaymentState($payment, [$payment_state]);

    $payment_method = $payment->getPaymentMethod();
    if (empty($payment_method)) {
      throw new InvalidArgumentException('The provided payment has no payment method referenced.');
    }

    switch ($payment_state) {
      case 'new':
        if ($payment_method->isExpired()) {
          throw new HardDeclineException('The provided payment method has expired.');
        }

        break;

      case 'authorization':
        if ($payment->isExpired()) {
          throw new \InvalidArgumentException('Authorizations are guaranteed for up to 29 days.');
        }
        if (empty($payment->getRemoteId())) {
          throw new \InvalidArgumentException('Could not retrieve the transaction ID.');
        }
        break;
    }
  }

}
